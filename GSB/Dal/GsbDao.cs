﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace GSB.Dal
{
    class GsbDao {

        private ConnexionSql connexionSql = ConnexionSql.getInstance("localhost", "gsb", "root", "");

        public DataTable getFiche() {

            List<string> fiches = new List<string>();
            DataTable dt = new DataTable();
            connexionSql.openConnection();
            MySqlCommand cmd = new MySqlCommand("SELECT mois, idEtat from fichefrais", connexionSql.getConnection());
            cmd.Prepare();

            MySqlDataReader reader = cmd.ExecuteReader();

            for (int i = 0; i < reader.FieldCount; i++) {
                dt.Columns.Add(reader.GetName(i));
            }

            while (reader.Read()){

                DataRow dr = dt.NewRow();

                for (int i = 0; i < reader.FieldCount; i++)
                {

                    dr[i] = reader.GetValue(i);
                }

                dt.Rows.Add(dr);
            }
            reader.Close();
            connexionSql.closeConnection();

            return dt;
        }

        public void updateFicheRB(string mois) {
            MySqlCommand cmd = new MySqlCommand("UPDATE fichefrais SET idEtat = @val WHERE mois = @val2", connexionSql.getConnection());
            cmd.Parameters.AddWithValue("@val", "RB");
            cmd.Parameters.AddWithValue("@val2", mois);
            cmd.Prepare();
        }

        public void updateFicheCL(string mois)
        {
            MySqlCommand cmd = new MySqlCommand("UPDATE fichefrais SET idEtat = @val WHERE idEtat = @val2 AND mois = @val3", connexionSql.getConnection());
            cmd.Parameters.AddWithValue("@val", "RB");
            cmd.Parameters.AddWithValue("@val2", "CL");
            cmd.Parameters.AddWithValue("@val3", mois);
            cmd.Prepare();
        }


    }
}
