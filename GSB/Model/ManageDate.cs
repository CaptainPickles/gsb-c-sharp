﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB.Model
{
    class ManageDate {
        public ManageDate() {

        }
        public DateTime getTodayDate() {
            DateTime today = new DateTime();
            today = DateTime.Now;
            Console.WriteLine("La date du jour est :" + today.ToString());

            return today;
        }

        public int getMonth(DateTime today) {
            int month = today.Month;
            Console.WriteLine("Le mois du jour est : " + month.ToString());

            return month;
        }

        public int getDay(DateTime today) {
            int todayDate = today.Day;
            Console.WriteLine("La date du jour est :" + todayDate.ToString());
            return todayDate;
        }

        public string getPreviousMonth(DateTime today) {
            var month = new DateTime(today.Year, today.Month, 1);
            var first = month.AddMonths(-1);
            string monthString = first.Month.ToString();
            if (first.Month <= 9)
            {
                monthString = "0";
                monthString += first.Month.ToString();
            }
                string PreviousMonth = today.Year + "" + monthString;
            Console.WriteLine("Le mois précédent est:" + PreviousMonth.ToString());

            return PreviousMonth;
        }
        public DateTime extractDateTimeFromQuerry(string Date)
        {
            int startIndex = 0;
            int length = 4;
            int startIndex2 = 4;
            int length2 = 2;
            string anné = Date.Substring(startIndex, length);
            string mois = Date.Substring(startIndex2, length2);
            int anné1 = Int16.Parse(anné);
            int mois1 = Int16.Parse(mois);

            DateTime date1 = new DateTime(anné1, mois1, 1);

           Console.WriteLine(date1);

            return date1; 
        }      
    }
}
